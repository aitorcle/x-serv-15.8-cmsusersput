
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('loggedin', views.loggedin, name='loggedin'),
    path('logout', views.user_logout, name='logout'),
    path('<short>', views.redirect, name='redirect')

]
