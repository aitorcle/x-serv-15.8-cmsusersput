from django.contrib.auth import logout
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Lista
import string
import random


@csrf_exempt


def index(request):
    lista = Lista.objects.all()
    if request.method == 'GET':
        response = render(request, 'pages/index.html', {'Lista': lista})
    if request.method == 'POST':
        if request.user.is_authenticated:
            cuerpost = request.POST['url']
            if cuerpost[0:7] != 'http://' and cuerpost[0:8] != 'https://':
                response = HttpResponseNotFound("La direccion no es una url valida, debe empezar por http:// o https://")
            else:
                try:
                    Lista.objects.get(url=cuerpost)
                    response = HttpResponseRedirect("/")
                except Lista.DoesNotExist:
                    short = '/' + (''.join(random.choices(string.ascii_lowercase + string.digits, k=5)))
                    u = Lista(url=cuerpost, short=short)
                    u.save()
                    response = render(request, 'pages/redireccion.html',
                                      {'url': u.url, 'short': short})
        else:
            response = HttpResponse('Debes estar logeado para acortar direcciones, <a href="/login">Login</a>')
    return response


def redirect(request, short):
    if request.method == 'GET':
        corta = '/' + short
        try:
            s = Lista.objects.get(short=corta)
            return HttpResponseRedirect(s.url)
        except Lista.DoesNotExist:
            return HttpResponseNotFound("La pagina " + short + " no existe")


def loggedin(request):
    if request.user.is_authenticated:
        response = 'Logged in as ' + request.user.username
    else:
        response = 'Not login in. <a href="/login">Login</a>'
    return HttpResponse(response)

def user_logout(request):
    logout(request)
    return HttpResponse('Ha deslogeado correctamente, <a href="/">Volver a la página principal</a>')
